from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('', views.showalldata, name = 'showalldata'),
    path('new_post/' , views.addblog , name='addblog')
    

]