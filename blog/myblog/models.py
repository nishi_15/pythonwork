from django.db import models

# Create your models here.

class ContentBlog(models.Model):
    content_heading = models.CharField(max_length=10000)
    content_data = models.CharField(max_length=100000000)
    content_date = models.DateTimeField('date published')

    def __str__(self):
        return self.content_heading + ' , ' + self.content_data


