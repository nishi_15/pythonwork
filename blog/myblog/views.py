from django.shortcuts import render

from django.http import HttpResponse, HttpResponseRedirect

from .models import ContentBlog

# Create your views here.


# Show all th blogs available
def showalldata(request):
    content_list = ContentBlog.objects.order_by('-content_date')
    context ={
        'content_list' : content_list,
    }
    return render(request,'myblog/index.html',context)


def addblog(request):
    print("=--------------------debug1----------------")
    if request.method == 'POST':
        print("=--------------------debug2----------------")
        if request.POST.get('title'):
            print("=--------------------debug3----------------")
            blog = ContentBlog()
            blog.content_heading = request.POST.get('title')
            blog.content_data = request.POST.get("blog_data")
            blog.content_date = request.POST.get("date_pub")

            blog.save()            
            return render(request,'myblog/newpost.html')
        else:
            print("=--------------------debug4----------------")
            return render(request,'myblog/newpost.html')
    else:
        print("=--------------------debug5----------------")
        return render(request,'myblog/newpost.html')

