from django.contrib import admin

# Register your models here.

from .models import ContentBlog

admin.site.site_header = 'MyBlog Admin'
admin.site.site_title = 'MyBlog'
admin.site.index_title = 'MyBlog Administration'

admin.site.register(ContentBlog)
