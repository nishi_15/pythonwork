from django.shortcuts import render


from .models import yourCustomData 

# Create your views here.

def index(request):
    return render(request,'todos/index.html')

def showmenu(request):
    data_list = yourCustomData.objects.all()
    return render(request,'todos/showmenu.html',{'mywork':data_list})

### to add data

def additem(request):
    print("-------------------------------",request.method,"---------------------------")
    if request.method == 'POST':
        print("---------------------------",request.POST.get('work'),"--------------------")
        if request.POST.get('work'):
            post = yourCustomData()
            post.data = request.POST.get('work')
            post.save()   
            print("--------------------debug 1---------------")     
            return render(request,'todos/additem.html')
        else:
            print("--------------------debug 2---------------")   
            return render(request,'todos/additem.html') 
    else:
        return render(request,'todos/additem.html')
