from django.contrib import admin

from .models import yourCustomData

admin.site.site_header = 'ToDoList Admin'
admin.site.site_title = 'ToDoList Admin Area'
admin.site.index_title = ' ToDoList Admin'

admin.site.register(yourCustomData)

# Register your models here.
