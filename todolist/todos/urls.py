from django.urls import path

from . import views

urlpatterns = [
    path('',views.index ,name='index'),
    path('show/',views.showmenu,name='showmenu'),
    path('additem/',views.additem,name='additem'),
    path('showmenu/',views.showmenu ,name='showmenu')
]