from django.contrib import admin

# Register your models here.

from .models import Question,Choice

### change the site header obviously !!

admin.site.site_header = 'Pollster Admin'
admin.site.site_title = 'Pollster Admin Area'
admin.site.index_title = ' Pollster Admin'

### to see choices with question we are gng to use tabular inline 
class ChoiceInline(admin.TabularInline):
	model = Choice
	extra = 5

class QuestionAdmin(admin.ModelAdmin):
	fieldset = [(None , {'fields': ['question_text']}),
	('Date Information', {'fields':['pub_date'],'classes':['collapse']}),]
	inlines = [ChoiceInline]

# admin.site.register(Question)
# admin.site.register(Choice)

admin.site.register(Question,QuestionAdmin)
